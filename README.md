# Visado de Proyectos

## Sistema de proyectos
**Proyecto open source.**

## Sistema Operativo

- GNU/Linux (Ubuntu 20.04.1 LTS)

## Requerimientos

- PHP 7.3.19
- MariaDB 10.3.25
- Composer 2.0.8
- Node.JS v14.15.5
- Sass 1.32.7

## Framework

- CodeIgniter4 (Version 4.1.1)
- Bootstrap5 (Version 5.0.0)

## Instalación

Para descargar o clonar el proyecto:
<https://gitlab.com/JimmyMayta/SIB-Visados.git>

    jimmy@JM:~/Escritorio$ git clone https://gitlab.com/JimmyMayta/SIB-Visados.git

Creando la Base de datos

    MariaDB [(none)]> CREATE USER 'visados'@'localhost' IDENTIFIED BY '67429923';
    MariaDB [(none)]> CREATE DATABASE visadosdatabase CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';
    MariaDB [(none)]> GRANT ALL PRIVILEGES ON visadosdatabase.* TO 'visados'@'localhost';
    MariaDB [(none)]> GRANT GRANT OPTION ON visadosdatabase.* TO 'visados'@'localhost';
    MariaDB [(none)]> FLUSH PRIVILEGES;

Base de datos

    jimmy@JM:~$ mysql -u visados -h localhost -p67429923
    MariaDB [(none)]> USE visadosdatabase;
    MariaDB [visadosdatabase]>


Aplicando Migraciones

    jimmy@JM:~/Escritorio/SIB-Visados$ php spark migrate

Desplegar proyecto

    jimmy@JM:~/Escritorio/SIB-Visados$ php spark serve

## Restricciones
1. Desarrollado solo para Bolivia
2. Unico idioma español
3. Solo controla proyectos

## Recientes actualizaciones importantes
| Fecha | Descripción |
|-|-|
| 22/03/2021 | Inicio de proyecto |
| 19/04/2021 | Proyecto finalizado (SIB-Visados) |

## Sass

    jimmy@JM:~/Escritorio/SIB-Visados/public$ sass sass/app.scss css/app.css --style=compressed --watch

- PHP 7.3.19
- MariaDB 10.3.25
- Composer 2.0.8
- Node.JS

## spark
    php spark cache:clear
    php spark cache:info
    php spark debugbar:clear
    php spark logs:clear

***
![Imagen](public/images/20210326220001.svg "")

![Imagen](records/Database.png "")

    (base) jimmy@JM:~/ProjectsPHP/SIB-Visados$
***
