<header class="blog-header py-3">
  <div class="row flex-nowrap justify-content-center align-items-center">
    <div class="col-12 text-center">
      <a class="blog-header-logo text-white" href="<?= route_to('principal') ?>">
        <i class="bi bi-dot" style="font-size: 2rem; color: #ffffff;"></i>SIB - Visado de Proyectos<a href="<?= route_to('login') ?>"><i class="bi bi-dot" style="font-size: 2rem; color: #ffffff;"></i></a>
      </a>
    </div>
  </div>
</header>
