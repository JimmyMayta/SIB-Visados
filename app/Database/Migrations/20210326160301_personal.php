<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;
use App\Libraries\App;

class Personal extends Migration {
  public function up() {
    $this->db->enableForeignKeyChecks();

    $this->forge->addField([
      'id' => ['type' => 'INT', 'constraint' => 9, 'unsigned' => true, 'auto_increment' => true],
      'code' => ['type' => 'VARCHAR', 'constraint' => '30'],
      'names' => ['type' => 'VARCHAR', 'constraint' => '90'],
      'firstlastname' => ['type' => 'VARCHAR', 'constraint' => '90'],
      'secondlastname' => ['type' => 'VARCHAR', 'constraint' => '90'],
      'email' => ['type' => 'VARCHAR', 'constraint' => '90'],

      'user_id' => ['type' => 'INT', 'constraint' => 9, 'unsigned' => true],
      'group_id' => ['type' => 'INT', 'constraint' => 9, 'unsigned' => true],
      'permission_id' => ['type' => 'INT', 'constraint' => 9, 'unsigned' => true],
      'state_id' => ['type' => 'INT', 'constraint' => 9, 'unsigned' => true],

      'description' => ['type' => 'VARCHAR', 'constraint' => '300', 'null' => true],
      'creationdate' => ['type' => 'DATETIME', 'null' => true],
      'upgradedate' => ['type' => 'DATETIME', 'null' => true],
      'eliminationdate' => ['type' => 'DATETIME', 'null' => true]
    ]);
    $this->forge->addKey('id', true);
    $this->forge->addForeignKey('user_id', 'users', 'id');
    $this->forge->addForeignKey('group_id', 'groups', 'id');
    $this->forge->addForeignKey('permission_id', 'permissions', 'id');
    $this->forge->addForeignKey('state_id', 'state', 'id');
    $this->forge->createTable('personal');

    $db = \Config\Database::connect();


    $app = new App();

    $sql = "INSERT INTO personal (code, names, firstlastname, secondlastname, email, user_id, group_id, permission_id, state_id, creationdate) VALUES (?, ?, ?, ?, ?, ?,? , ?, ?, ?)";
    $db->query($sql, [
      $app->code(), 'Admin', 'Admin', 'Admin', 'admin@admin.com', 1, 1, 1, 1, '2021-03-26 10:09:01'
    ]);

    $sql = "INSERT INTO personal (code, names, firstlastname, secondlastname, email, user_id, group_id, permission_id, state_id, creationdate) VALUES (?, ?, ?, ?, ?, ?,? , ?, ?, ?)";
    $db->query($sql, [
      $app->code(), 'Personal', 'Personal', 'Personal', 'personal@personnal.com', 1, 1, 1, 1, '2021-03-26 10:09:01'
    ]);
  }

  public function down() {
    $this->forge->dropTable('personal');
  }
}











