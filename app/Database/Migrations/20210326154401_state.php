<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class State extends Migration {
  public function up() {
    $this->forge->addField([
      'id' => ['type' => 'INT', 'constraint' => 9, 'unsigned' => true, 'auto_increment' => true],
      'state' => ['type' => 'VARCHAR', 'constraint' => '30'],
      'description' => ['type' => 'VARCHAR', 'constraint' => '300', 'null' => true],
      'creationdate' => ['type' => 'DATETIME', 'null' => true],
      'upgradedate' => ['type' => 'DATETIME', 'null' => true],
      'eliminationdate' => ['type' => 'DATETIME', 'null' => true]
    ]);
    $this->forge->addKey('id', true);
    $this->forge->createTable('state');

    $db = \Config\Database::connect();
    $sql = "INSERT INTO state (state, creationdate) VALUES ('Act', '2021-03-26 10:09:01')";
    $db->query($sql);
    $sql = "INSERT INTO state (state, creationdate) VALUES ('Des', '2021-03-26 10:09:01')";
    $db->query($sql);
  }

  public function down() {
    $this->forge->dropTable('state');
  }
}











