<?php
namespace App\Libraries;
use DateTime;
use DateTimeZone;

class App {
  public function code() {
    $DateTime = new DateTime('now', new DateTimeZone('America/La_Paz'));
    return $DateTime->format('YmdHisu');
  }
}